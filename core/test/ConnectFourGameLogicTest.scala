import org.scalatest._

class VierGewinntTest extends FlatSpec with Matchers {
  import com.lmu.scala.mcts.games.connectfour.logic._
  import com.lmu.scala.mcts.games.connectfour.logic.GameLogic._

  "initial state" should "allow every movements" in {
    movements(initialState()) should be (Set(0, 1, 2, 3, 4, 5, 6))
  }

  "after one movement" should "every movements still be allowed" in {
    val ms = play(1, initialState(), 0).map(movements)
    ms should be (Some(Set(0, 1, 2, 3, 4, 5, 6)))
  }

  "after six movements on the same column" should
  "this column not be allowed anymore" in {
    val sOpt = Some(initialState())
      .flatMap(s => play(1, s, 0))
      .flatMap(s => play(2, s, 0))
      .flatMap(s => play(1, s, 0))
      .flatMap(s => play(2, s, 0))
      .flatMap(s => play(1, s, 0))
      .flatMap(s => play(2, s, 0))
    sOpt.map(movements) should be (Some(Set(1, 2, 3, 4, 5, 6)))
    sOpt.flatMap(s => play(1, s, 0)) should be (None)
  }

  "A near win/lost" should "be recognized for player 1" in {
    val sOpt = Some(initialState())
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
    sOpt.map(s => rateState(1, s)) should be (Some(Rate(1)))
    sOpt.map(s => rateState(2, s)) should be (Some(Rate(-1)))
  }

  "A won/lost game" should "be recognized for player 1" in {
    val sOpt = Some(initialState())
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
    sOpt.map(s => rateState(1, s)) should be (Some(Won))
    sOpt.map(s => rateState(2, s)) should be (Some(Lost))
  }

  "A won/lost game" should "be recognized for player 2" in {
    val sOpt = Some(initialState())
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
      .flatMap(s => play(1, s, 1))
      .flatMap(s => play(2, s, 2))
    sOpt.map(s => rateState(2, s)) should be (Some(Won))
    sOpt.map(s => rateState(1, s)) should be (Some(Lost))
  }
}

