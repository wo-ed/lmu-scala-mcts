import org.scalatest._

class PacmanTest extends FlatSpec with Matchers {
  import com.lmu.scala.mcts.games.pacman.logic._
  import com.lmu.scala.mcts.games.pacman.logic.GameLogic._

  "parsingWall" should "works" in {
    parseWalls("") should be (Set())
    parseWalls(" ") should be (Set())
    parseWalls("  \n  ") should be (Set())
    parseWalls("   \n   \n   ") should be (Set())

    parseWalls("#") should be (Set((0,0)))
    parseWalls("  \n# ") should be (Set((0,0)))
    parseWalls("   \n   \n#  ") should be (Set((0,0)))

    parseWalls("# \n# ") should be (Set((0,0), (0,1)))
    parseWalls("#  \n#  \n#  ") should be (Set((0,0), (0,1), (0,2)))
  }

  val board1 = "."

  board1 should "allow all movements" in {
    val walls = parseWalls(board1)
    val p = (0.5f,0.5f)
    val e = 0.1f
    walls should be (Set())
    movementAllowed(e, 1, 1, p, walls, Left)  should be (true)
    movementAllowed(e, 1, 1, p, walls, Up)    should be (true)
    movementAllowed(e, 1, 1, p, walls, Right) should be (true)
    movementAllowed(e, 1, 1, p, walls, Down)  should be (true)
  }

  val board2 = ".#\n##"

  board2 should "not allow any movements" in {
    val walls = parseWalls(board2)
    val p = (0.5f,1.5f)
    val e = 0.1f
    walls should be (Set((0,0), (1,0), (1,1)))
    movementAllowed(e, 2, 2, p, walls, Left)  should be (false)
    movementAllowed(e, 2, 2, p, walls, Up)    should be (false)
    movementAllowed(e, 2, 2, p, walls, Right) should be (false)
    movementAllowed(e, 2, 2, p, walls, Down)  should be (false)
  }

  val board3 = "..\n##"

  board3 should "allow left and right" in {
    val walls = parseWalls(board3)
    val p = (0.5f,1.5f)
    val e = 0.1f
    walls should be (Set((0,0), (1,0)))
    movementAllowed(e, 2, 2, p, walls, Left)  should be (true)
    movementAllowed(e, 2, 2, p, walls, Up)    should be (false)
    movementAllowed(e, 2, 2, p, walls, Right) should be (true)
    movementAllowed(e, 2, 2, p, walls, Down)  should be (false)
  }

  val board4 = "###\n#.#\n###"

  board4 should "not allow any movements" in {
    val walls = parseWalls(board4)
    val p = (1.5f,1.5f)
    val e = 0.1f
    walls should be (Set(
      (0,2), (1,2), (2,2),
      (0,1),        (2,1),
      (0,0), (1,0), (2,0)
    ))
    movementAllowed(e, 3, 3, p, walls, Left)  should be (false)
    movementAllowed(e, 3, 3, p, walls, Up)    should be (false)
    movementAllowed(e, 3, 3, p, walls, Right) should be (false)
    movementAllowed(e, 3, 3, p, walls, Down)  should be (false)
  }

  val board5 = "#.#\n...\n#.#"

  board5 should "allow some movements" in {
    val walls = parseWalls(board5)
    val e = 0.1f
    walls should be (Set((0,2), (2,2), (0,0), (2,0)))

    movementAllowed(e, 3, 3, (1.5f,1.5f), walls, Left)  should be (true)
    movementAllowed(e, 3, 3, (1.5f,1.5f), walls, Up)    should be (true)
    movementAllowed(e, 3, 3, (1.5f,1.5f), walls, Right) should be (true)
    movementAllowed(e, 3, 3, (1.5f,1.5f), walls, Down)  should be (true)

    movementAllowed(e, 3, 3, (0.5f,1.5f), walls, Left)  should be (true)
    movementAllowed(e, 3, 3, (0.5f,1.5f), walls, Up)    should be (false)
    movementAllowed(e, 3, 3, (0.5f,1.5f), walls, Right) should be (true)
    movementAllowed(e, 3, 3, (0.5f,1.5f), walls, Down)  should be (false)
  }
}

