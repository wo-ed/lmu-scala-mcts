package com.lmu.scala.mcts.main

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.tiled.{TiledMap, TmxMapLoader}
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.{Game, Gdx, InputMultiplexer}
import com.lmu.scala.mcts.gdxextensions.AssetManagerImplicits._

object MonteCarloTreeSearchGame {
  val DefaultBgColor = new Color(0.1f, 0.1f, 0.1f, 1.0f)
}

class MonteCarloTreeSearchGame extends Game {

  // We need lazy here because the framework is not loaded yet.
  lazy val batch: SpriteBatch = new SpriteBatch
  lazy val assetManager: AssetManager = new AssetManager
  lazy val defaultSkin: Skin = assetManager.get(StandardAssets.defaultSkin)
  lazy val inputMultiplexer = new InputMultiplexer

  private var initialized = false

  override def create(): Unit = {
    assetManager.setLoader(classOf[TiledMap], new TmxMapLoader)
    assetManager.load(StandardAssets)
    Gdx.input.setInputProcessor(inputMultiplexer)
    Gdx.input.setCatchBackKey(true)
  }

  override def render(): Unit =  {
    if(!initialized && assetManager.update) {
      initialize()
    }
    super.render()
  }

  def initialize(): Unit = {
    setScreen(new MainMenuScreen(this))
    initialized = true
  }

  override def dispose(): Unit = {
    super.dispose()
    batch.dispose()
    assetManager.dispose()
  }
}