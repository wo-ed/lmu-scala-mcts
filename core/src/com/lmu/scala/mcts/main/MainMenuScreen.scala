package com.lmu.scala.mcts.main

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.{Button, Table, TextButton}
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.{Gdx, InputAdapter, Screen, ScreenAdapter}
import com.lmu.scala.mcts.games.connectfour.{ConnectFourAssets, ConnectFourScreen}
import com.lmu.scala.mcts.games.pacman.{PacManAssets, PacManScreen}
import com.lmu.scala.mcts.gdxextensions.AssetManagerImplicits._
import com.lmu.scala.mcts.gdxextensions.Clearable
import com.lmu.scala.mcts.gdxextensions.input.ListenerImplicits._
import com.lmu.scala.mcts.main.MonteCarloTreeSearchGame._

class MainMenuScreen(game: MonteCarloTreeSearchGame) extends ScreenAdapter with Clearable {
  override protected var bgColor: Color = DefaultBgColor

  val inputAdapter = new InputAdapter {
    override def keyDown(keycode: Int): Boolean = keycode match {
      case Keys.BACK | Keys.BACKSPACE | Keys.ESCAPE => Gdx.app.exit(); true
      case _ => false
    }
  }

  private val stage = new Stage(new ScreenViewport, game.batch)

  private val uiTable = new Table(game.defaultSkin)
  uiTable.setFillParent(true)

  private var clickedButton: Option[Button] = None

  private val connectFourButton = new TextButton("Connect Four", game.defaultSkin)
  connectFourButton.addClickListener((_, _, _) => {
    if (clickedButton.isEmpty) {
      game.assetManager.load(ConnectFourAssets)
      clickedButton = Some(connectFourButton)
    }
  })

  private val exitButton = new TextButton("Exit", game.defaultSkin)
  exitButton.addClickListener((_, _, _) => Gdx.app.exit())

  private val pacManButton = new TextButton("PacMan", game.defaultSkin)
  pacManButton.addClickListener((_, _, _) => {
    if (clickedButton.isEmpty) {
      game.assetManager.load(PacManAssets)
      clickedButton = Some(pacManButton)
    }
  })

  addActors()

  private def addActors(): Unit = {
    stage.addActor(uiTable)
    uiTable.add(connectFourButton)
    uiTable.row()
    uiTable.add(pacManButton)
    uiTable.row()
    uiTable.add(exitButton)
  }

  override def render(delta: Float): Unit = {
    if (game.assetManager.update) {
      showNextScreen()
    }
    clearScreen()
    stage.act(delta)
    stage.draw()
  }

  private def showNextScreen(): Unit = nextScreen.foreach(s => {
    dispose()
    game.setScreen(s)
  })

  private def nextScreen: Option[Screen] = clickedButton.map {
    case `connectFourButton` => new ConnectFourScreen(game)
    case `pacManButton` => new PacManScreen(game)
  }

  override def resize(width: Int, height: Int): Unit = stage.getViewport.update(width, height, true)

  override def show(): Unit = {
    game.inputMultiplexer.addProcessor(inputAdapter)
    game.inputMultiplexer.addProcessor(stage)
  }

  override def dispose(): Unit = {
    game.inputMultiplexer.removeProcessor(inputAdapter)
    game.inputMultiplexer.removeProcessor(stage)
    stage.dispose()
  }
}
