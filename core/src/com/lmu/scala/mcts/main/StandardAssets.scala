package com.lmu.scala.mcts.main

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.lmu.scala.mcts.gdxextensions.Assets

object StandardAssets extends Assets {
  val defaultSkin = new AssetDescriptor[Skin]("menu/skins/starsoldier/star-soldier-ui.json", classOf[Skin])

  override val descriptors: List[AssetDescriptor[_]] = defaultSkin :: Nil
}
