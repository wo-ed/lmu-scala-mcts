package com.lmu.scala.mcts.games.pacman

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.{Color, OrthographicCamera}
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.{Intersector, Rectangle}
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.{Button, ButtonGroup, HorizontalGroup, TextButton, VerticalGroup}
import com.badlogic.gdx.utils.{Align, Pools}
import com.badlogic.gdx.utils.viewport.{FitViewport, ScreenViewport}
import com.badlogic.gdx.{InputAdapter, ScreenAdapter}
import com.lmu.scala.mcts.games.pacman.logic.GameLogic
import com.lmu.scala.mcts.games.pacman.actors._
import com.lmu.scala.mcts.games.pacman.logic._
import com.lmu.scala.mcts.gdxextensions.AssetManagerImplicits._
import com.lmu.scala.mcts.gdxextensions.Clearable
import com.lmu.scala.mcts.main.{MainMenuScreen, MonteCarloTreeSearchGame}
import com.lmu.scala.mcts.planner.{FPlanner, MctsPlanner, State}

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class PacManScreen(game: MonteCarloTreeSearchGame)
  extends ScreenAdapter
    with Clearable
    with TileValidator {

  private val map: PacManMap = new PacManMap(game.assetManager)
  override protected var bgColor: Color = Color.BLACK
  private val shortestPath = ListBuffer[Node]()
  private val mapRenderer = new OrthogonalTiledMapRenderer(map.tiledMap, game.batch)
  private val drawTriggerShapesEnabled = false
  private val shapeRenderer: ShapeRenderer = new ShapeRenderer()
  shapeRenderer.setAutoShapeType(true)
  shapeRenderer.setColor(Color.GREEN)

  private val camera: OrthographicCamera = new OrthographicCamera
  private val viewport = new FitViewport(map.width, map.height, camera)

  private val gameStage = new Stage(viewport, game.batch)
  private val uiStage = new Stage(new ScreenViewport, game.batch)

  private val mctsButton = new TextButton("MCTS", game.defaultSkin)
  private val aStarButton = new TextButton("A*", game.defaultSkin)
  private val aStarWithScatteringButton = new TextButton("A* with scattering  ", game.defaultSkin)

  private val verticalGroup = new VerticalGroup()
  verticalGroup.align(Align.left)
  verticalGroup.addActor(mctsButton)
  verticalGroup.addActor(aStarButton)
  verticalGroup.addActor(aStarWithScatteringButton)
  verticalGroup.setFillParent(true)
  uiStage.addActor(verticalGroup)

  // Radio button functionality: only one button but always one is checked
  private val buttonGroup = new ButtonGroup[TextButton]()
  buttonGroup.add(mctsButton)
  buttonGroup.add(aStarButton)
  buttonGroup.add(aStarWithScatteringButton)

  // Determines the algorithm that is used at first
  aStarWithScatteringButton.setChecked(true)

  private val pacMan = new PacMan(map, game.assetManager)

  var walls = Set[(Int,Int)]()
  for (x <- 0 until map.wallRectangles.length) {
    for (y <- 0 until map.wallRectangles(x).length) {
      if (!map.wallRectangles(x)(y).isEmpty) {
        walls = walls + ((x, y))
      }
    }
  }
  val width = map.columns;
  val height = map.rows;
  var pacman = (pacMan.triggerX / map.tileWidth, pacMan.triggerY / map.tileWidth)
  println(GameLogic.wallsToString(width, height, walls, pacman))
  var frameCount = 0

  // The direction that the user tries to navigate PacMan in
  private var wantedPacManDirection: Option[Direction] = None

  private val ghosts =
    new Ghost(GhostType.Red, map, game.assetManager) ::
    new Ghost(GhostType.Pink, map, game.assetManager) ::
    new Ghost(GhostType.Cyan, map, game.assetManager) ::
    new Ghost(GhostType.Orange, map, game.assetManager) ::
    Nil

  private var food: List[Food] = map.createFoodObjects()

  private val inputAdapter = new InputAdapter {
    override def keyDown(keycode: Int): Boolean = keycode match {
      case Keys.LEFT =>
        wantedPacManDirection = Some(Left)
        true
      case Keys.RIGHT =>
        wantedPacManDirection = Some(Right)
        true
      case Keys.DOWN =>
        wantedPacManDirection = Some(Down)
        true
      case Keys.UP =>
        wantedPacManDirection = Some(Up)
        true
      case Keys.BACK | Keys.BACKSPACE | Keys.ESCAPE =>
        exit()
        true
      case _ =>
        false
    }
  }

  resetGame()

  private def exit(): Unit = {
    dispose()
    game.setScreen(new MainMenuScreen(game))
  }

  private def resetGame(): Unit = {
    gameStage.clear()

    food = map.createFoodObjects()
    food.foreach(gameStage.addActor)

    ghosts.foreach(g => {
      g.reset()
      gameStage.addActor(g)
    })

    pacMan.reset()
    gameStage.addActor(pacMan)

    wantedPacManDirection = None
  }

  override def isValidPathTile(col: Int, row: Int): Boolean = {
    col >= 0 && col < map.columns &&
      row >= 0 && row < map.rows &&
      map.wallRectangles(col)(row).isEmpty
  }

  override def render(delta: Float): Unit = {
    clearScreen()

    game.batch.setColor(Color.WHITE)

    frameCount += 1

    renderMap()
    renderStage(gameStage, delta)

    //teleportIfOutsideOfViewport(pacMan)
    ghosts.foreach(teleportIfOutsideOfViewport)

    pacman = (
      0.5f + pacMan.triggerX / map.tileWidth,
      0.5f + pacMan.triggerY / map.tileWidth
    )
    val epsilon = 0.0625f
    //val epsilon = 0.03125f
    //val epsilon = 0.015625f
    //val epsilon = delta
    val allowed = (d: Direction) =>
      GameLogic.movementAllowed(epsilon, width, height, pacman, walls, d)

    val wanDir = wantedPacManDirection.filter(allowed)
    pacMan.direction = wanDir match {
      case None => pacMan.direction.filter(allowed)
      case Some(d) => Some(d)
    }

    /*
    println(
      s"epsilon = $epsilon, pacman = $pacman, direction = ${pacMan.direction.toString}")
    */

    pacMan.direction.foreach(d => {
      val (x, y) = GameLogic.move(epsilon, width, height, pacman, d)
      val (xx, yy) = (x - 0.5f, y - 0.5f)
      val (xxx, yyy): (Float,Float) = d match {
        case Left | Right => (xx, yy.round.toFloat)
        case Up   | Down  => (xx.round.toFloat, yy)
      }
      pacMan.triggerX = xxx * map.tileWidth
      pacMan.triggerY = yyy * map.tileWidth
    })

    ghosts.foreach(ghost => {
      ghost.directionalVelocity = epsilon * map.tileWidth
      val p = (
        0.5f + ghost.triggerX / map.tileWidth,
        0.5f + ghost.triggerY / map.tileWidth
      )

      if (isTriggerExactlyOnATile(ghost)) {
        val nextDirection = if (aStarWithScatteringButton.isChecked) {
          getGhostDirectionFromShortestPath(ghost, scatteringEnabled = true)
        } else if (aStarButton.isChecked) {
          getGhostDirectionFromShortestPath(ghost, scatteringEnabled = false)
        } else {
          None
        }
        nextDirection.foreach(d => {
          if (GameLogic.movementAllowed(delta, width, height, p, walls, d)) {
            ghost.direction = Some(d)
          }
        })
      }

      if(mctsButton.isChecked) {

        type A = Direction
        type S = ((Float, Float), (Float, Float))
        type S_ = State.Adapter[S, A]

        val planner: FPlanner[S, A] = new MctsPlanner[S_, A](
          iterations = 1000,
          horizon = 15,
          new MctsPlanner.BellmanUpdateStrategy[S_, A](5, 1.0),
          MctsPlanner.uctPolicy[S_, A](100.0),
          MctsPlanner.uctPolicy[S_, A](0.0),
          MctsPlanner.uniformSamplePolicy[S_, A],
          true)

        if (frameCount % 60 == 0 /*isTriggerExactlyOnATile(ghost)*/ ) {

          val future: Future[Option[Direction]] = Future {
            val reward: S => Float = {
              case ((px, py), (gx, gy)) =>
                math.sqrt(math.pow(px - gx, 2) + math.pow(py - gy, 2)).toFloat * -1.0f
            }

            val epsilon_ = 0.25f

            val actions: S => Seq[A] = {
              case (_, g) => GameLogic.movements(epsilon_, width, height, g, walls).to[Seq]
            }

            val successors: (S, A) => Seq[S] = {
              case ((p, g), a) =>
                val ghostMoved = GameLogic.move(epsilon_, width, height, g, a)

                GameLogic.movements(epsilon_, width, height, p, walls).map(ap =>
                  (GameLogic.move(epsilon_, width, height, p, ap), ghostMoved)).to[Seq]
            }

            //val nextDirection = getGhostDirectionFromShortestPath(ghost)
            planner.bestAction(reward, actions, successors, (pacman, p))
          }

          future.onSuccess {
            case None =>
            case Some(d) => {
              if (GameLogic.movementAllowed(delta, width, height, p, walls, d)) {
                ghost.direction = Some(d)
              }
            }
          }
        }
      }
    })

    // Collision detection happens here
    //moveByVelocity(pacMan)
    ghosts.foreach(moveByVelocity)

    val (eatenFood, uneatenFood) = food.partition(pacMan.overlaps)
    // Remove the eaten food from the gameStage
    eatenFood.foreach(_.remove())
    // Remember the uneaten food for overlap checks
    food = uneatenFood

    if (food.isEmpty) {
      resetGame()
    } else {
      val pacManRect = Pools.obtain(classOf[Rectangle])
      pacMan.toRectangle(pacManRect)

      ghosts.find(g => pacManRect.contains(g.centerX, g.centerY))
        .foreach(_ => resetGame())

      Pools.free(pacManRect)
    }

    if (drawTriggerShapesEnabled) {
      drawTriggerShapes()
    }

    buttonGroup.getButtons.items.foreach(button => {
      if(button != null) { button.setColor(if (button.isChecked) {
        Color.RED
      } else {
        Color.WHITE
      })}})

    renderStage(uiStage, delta)
  }

  private def isTriggerExactlyOnATile(trigger: Trigger): Boolean = {
    trigger.triggerX % map.tileWidth == 0 && trigger.triggerY % map.tileHeight == 0
  }

  /** Calculates the shortest Path to PacMan if the ghosts behavior is set to "chase"
    * or to a random goal if the behavior is set to "scatter"
    * @return the next direction in the shortest path to PacMan or a random goal
    */
  private def getGhostDirectionFromShortestPath(ghost: Ghost, scatteringEnabled: Boolean): Option[Direction] = {
    val (startCol, startRow) = map.convertPositionToTileIndices(ghost.triggerX, ghost.triggerY)
    val (endCol, endRow) =
      if(ghost.behavior == Behavior.Chase || !scatteringEnabled) {
        map.convertPositionToTileIndices(pacMan.triggerX, pacMan.triggerY)
      } else {
        ghost.scatterPoint
      }

    AStar.findShortestPath(startCol, startRow, endCol, endRow, this, shortestPath)
    val pathLength = shortestPath.size

    val result = if (pathLength >= 2) {
      val nextTile = shortestPath(1)
      if (nextTile.x < startCol) {
        Some(Left)
      } else if (nextTile.x > startCol) {
        Some(Right)
      } else if (nextTile.y > startRow) {
        Some(Up)
      } else if (nextTile.y < startRow) {
        Some(Down)
      } else {
        None
      }
    } else {
      None
    }

    shortestPath.foreach(Pools.free(_))
    result
  }

  /**
    * Teleports a game object to the other side of the viewport if it exited the viewport
    */
  private def teleportIfOutsideOfViewport(trigger: Trigger): Unit = {
    if (trigger.triggerX <= -trigger.triggerWidth) {
      trigger.triggerX = camera.viewportWidth
    } else if (trigger.triggerX >= camera.viewportWidth + trigger.triggerWidth) {
      trigger.triggerX = -trigger.triggerWidth
    }
  }

  /**
    * Moves a game object based on its velocity but only as far as there is no wall
    */
  private def moveByVelocity(gameObject: FourDirectionalMovingActor with Trigger): Unit = {
    val firstIntersection = Pools.obtain(classOf[Rectangle])
    val nextPos = Pools.obtain(classOf[Rectangle])
    gameObject.toRectangle(nextPos)

    nextPos.x += gameObject.velocityX
    nextPos.y += gameObject.velocityY

    if (map.intersectWithWall(nextPos, firstIntersection)) {

      val deltaX = gameObject.direction match {
        case Some(Left) => firstIntersection.width
        case Some(Right) => -firstIntersection.width
        case _ => 0.0f
      }
      nextPos.x += deltaX

      val deltaY = gameObject.direction match {
        case Some(Down) => firstIntersection.height
        case Some(Up) => -firstIntersection.height
        case _ => 0.0f
      }
      nextPos.y += deltaY
    }

    gameObject.triggerX = nextPos.x
    gameObject.triggerY = nextPos.y

    Pools.free(firstIntersection)
    Pools.free(nextPos)
  }

  private def drawTriggerShapes(): Unit = {
    shapeRenderer.setProjectionMatrix(camera.combined)
    shapeRenderer.begin()

    map.flattenedWallRectangles.foreach(r => shapeRenderer.rect(r.x, r.y, r.width, r.height))
    food.foreach(f => f.drawTriggerShape(shapeRenderer))
    ghosts.foreach(_.drawTriggerShape(shapeRenderer))
    pacMan.drawTriggerShape(shapeRenderer)

    shapeRenderer.end()
  }

  private def renderStage(stage: Stage, delta: Float): Unit = {
    stage.getViewport.apply(true)
    stage.act(delta)
    stage.draw()
  }

  private def renderMap(): Unit = {
    viewport.apply(true)
    mapRenderer.setView(camera)
    mapRenderer.render()
  }

  override def resize(width: Int, height: Int): Unit = {
    gameStage.getViewport.update(width, height, true)
    viewport.update(width, height, true)
  }

  override def show(): Unit = {
    game.inputMultiplexer.addProcessor(inputAdapter)
    game.inputMultiplexer.addProcessor(uiStage)
  }

  override def dispose(): Unit = {
    game.assetManager.unload(PacManAssets)
    game.inputMultiplexer.removeProcessor(inputAdapter)
    game.inputMultiplexer.removeProcessor(uiStage)
    shapeRenderer.dispose()
    mapRenderer.dispose()
    gameStage.dispose()
    uiStage.dispose()
  }
}
