package com.lmu.scala.mcts.games.pacman.actors

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor

/**
  * Triggers can be used for collision detection
  */
trait Trigger extends Actor {

  /**
    * The offset to the texture position
    */
  val triggerOffsetX: Float
  val triggerOffsetY: Float

  def triggerWidth: Float
  def triggerHeight: Float

  def triggerX: Float = getX + triggerOffsetX
  def triggerY: Float = getY + triggerOffsetY
  def triggerX_= (value: Float): Unit = setX(value - triggerOffsetX)
  def triggerY_= (value: Float): Unit = setY(value - triggerOffsetY)

  def overlaps(other: Trigger): Boolean =
    triggerX < other.triggerX + other.triggerWidth &&
      triggerX + triggerWidth > other.triggerX &&
      triggerY < other.triggerY + other.triggerHeight &&
      triggerY + triggerHeight > other.triggerY

  /**
    * Draws the outline of the trigger
    */
  def drawTriggerShape(renderer: ShapeRenderer): Unit = {
    renderer.rect(triggerX, triggerY, triggerWidth, triggerHeight)
  }

  def toRectangle: Rectangle = {
    toRectangle(new Rectangle)
  }

  def toRectangle(rect: Rectangle): Rectangle = {
    rect.x = triggerX
    rect.y = triggerY
    rect.width = triggerWidth
    rect.height = triggerHeight
    rect
  }

  def centerX: Float = triggerX + triggerWidth / 2.0f
  def centerY: Float = triggerY + triggerHeight / 2.0f
}
