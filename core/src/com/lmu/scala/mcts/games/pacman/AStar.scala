package com.lmu.scala.mcts.games.pacman

import com.badlogic.gdx.utils.Pool.Poolable
import com.badlogic.gdx.utils.Pools

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object AStar {

  val openNodes = mutable.HashSet[Node]()
  val closedNodes = mutable.HashSet[Node]()
  var successors = mutable.HashSet[Node]()

  /**
    * Finds the shortest path from start to end via the A* algorithm
    * The Nodes must must be freed when they're not used anymore
    */
  def findShortestPath(startCol: Int,
                       startRow: Int,
                       endCol: Int,
                       endRow: Int,
                       tileValidator: TileValidator,
                       shortestPath: ListBuffer[Node]): Seq[Node] = {

    var endNode: Option[Node] = None
    val startNode = Pools.obtain(classOf[Node])
    startNode.initialize(None, startCol, startRow)

    openNodes += startNode

    while (openNodes.nonEmpty && endNode.isEmpty) {
      val q = openNodes.minBy(_.f)
      openNodes -= q

      val someQ = Some(q)
      val node1 = Pools.obtain(classOf[Node])
      val node2 = Pools.obtain(classOf[Node])
      val node3 = Pools.obtain(classOf[Node])
      val node4 = Pools.obtain(classOf[Node])
      node1.initialize(someQ, q.x - 1, q.y)
      node2.initialize(someQ, q.x + 1, q.y)
      node3.initialize(someQ, q.x, q.y - 1)
      node4.initialize(someQ, q.x, q.y + 1)

      successors.clear()
      successors += node1
      successors += node2
      successors += node3
      successors += node4

      successors = successors.filter(s => tileValidator.isValidPathTile(s.x, s.y))

      successors.foreach(s => {

        if (endNode.isEmpty) {
          // successor.g = q.g + distance between successor and q
          s.g = q.g + math.sqrt(math.pow(math.abs(q.x - s.x), 2.0) + math.pow(math.abs(q.y - s.y), 2.0)).toFloat

          //successor.h = distance from goal to successor
          s.h = math.sqrt(math.pow(math.abs(endCol - s.x), 2.0) + math.pow(math.abs(endRow - s.y), 2.0)).toFloat

          s.f = s.g + s.h

          if (s.x == endCol && s.y == endRow) {
            val n = Pools.obtain(classOf[Node])
            n.initialize(someQ, s.x, s.y, s.f, s.g, s.h)
            endNode = Some(n)
          } else if (!openNodes.exists(n => n.x == s.x && n.y == s.y && n.f < s.f)
            && !closedNodes.exists(n => n.x == s.x && n.y == s.y && n.f < s.f)) {
            openNodes += s
          } else {
            Pools.free(s)
          }
        }
      })
      closedNodes += q
    }

    shortestPath.clear()
    var node: Option[Node] = endNode
    while (node.nonEmpty) {
      shortestPath.prepend(node.get)
      node = node.get.parent
    }

    openNodes --= shortestPath
    openNodes.foreach(Pools.free(_))
    openNodes.clear()
    closedNodes --= shortestPath
    closedNodes.foreach(Pools.free(_))
    closedNodes.clear()

    shortestPath
  }
}

trait TileValidator {
  def isValidPathTile(x: Int, y: Int): Boolean
}

class Node(var parent: Option[Node],
           var x: Int = 0,
           var y: Int = 0,
           var f: Float = 0.0f,
           var g: Float = 0.0f,
           var h: Float = 0.0f) extends Poolable {

  // We need this constructor for Poolable.reset() to work
  def this() = {
    this(None)
  }

  def initialize(parent: Option[Node],
                 x: Int,
                 y: Int,
                 f: Float = 0.0f,
                 g: Float = 0.0f,
                 h: Float = 0.0f): Unit = {
    this.parent = parent
    this.x = x
    this.y = y
    this.f = f
    this.g = g
    this.g = h
  }

  override def reset(): Unit = {
    parent = None
    x = 0
    y = 0
    f = 0.0f
    g = 0.0f
    h = 0.0f
  }
}
