package com.lmu.scala.mcts.games.pacman.logic

abstract class Direction
case object Left  extends Direction
case object Up    extends Direction
case object Right extends Direction
case object Down  extends Direction

object GameLogic {
  def parseWalls(str: String): Set[(Int,Int)] = {
    val lines = str.split("\n");
    var s = Set[(Int,Int)]()
    for (k <- 0 until lines.length) {
      for (x <- 0 until lines(k).length) {
        val y = lines.length - 1 - k
        if (lines(k)(x) == '#') {
          s = s + ((x,y))
        }
      }
    }
    return s
  }

  def wallsToString(
    width: Int,
    height: Int,
    walls: Set[(Int,Int)],
    position: (Float,Float)): String = {
    val builder = new StringBuilder(width * height)
    val xx = scala.math.round(position._1)
    val yy = scala.math.round(position._2)
    for (y <- height -1 to 0 by -1) {
      for (x <- 0 until width) {
        val char = if (walls contains (x, y)) {
          '#'
        } else if (x == xx && y == yy) {
          'O'
        } else {
          ' '
        }
        builder.append(char)
      }
      builder.append('\n')
    }
    builder.toString()
  }

  def move(
    epsilon: Float,
    width: Int,
    height: Int,
    position: (Float,Float),
    movement: Direction): (Float,Float) = {
    (position, movement) match {
      case ((x, y), Left)  => ((x + width - epsilon) % width, y)
      case ((x, y), Right) => ((x + epsilon) % width, y)
      case ((x, y), Up)    => (x, (y + epsilon) % height)
      case ((x, y), Down)  => (x, (y + height - epsilon) % height)
    }
  }

  def movementAllowed(
    epsilon: Float,
    width: Int,
    height: Int,
    position: (Float,Float),
    walls: Set[(Int,Int)],
    movement: Direction): Boolean = {

    val m = 0.5f - epsilon
    val n = 0.5f + epsilon

    def centered(k: Float) = { val kk = k % 1; m <= kk && kk <= n }
    def between(k: Float, m: Float, n: Float) = m <= k && k <= n
    def onWall(x: Float, y:Float, p: (Int,Int)) = p match {
      case (xwall, ywall) =>
        xwall < x && between(x - xwall, m, n) &&
        ywall < y && between(y - ywall, m, n)
    }

    (move(1.0f, width, height, position, movement), movement) match {
      case ((x, y), Left)  => centered(y) && !walls.exists(w => onWall(x,y,w))
      case ((x, y), Right) => centered(y) && !walls.exists(w => onWall(x,y,w))
      case ((x, y), Up)    => centered(x) && !walls.exists(w => onWall(x,y,w))
      case ((x, y), Down)  => centered(x) && !walls.exists(w => onWall(x,y,w))
    }
  }

  def movements(
    epsilon: Float,
    width: Int,
    height: Int,
    position: (Float,Float),
    walls: Set[(Int,Int)]): Set[Direction] = {

    var s = Set[Direction]()
    if (movementAllowed(epsilon, width, height, position, walls, Left)) {
      s = s + (Left)
    }
    if (movementAllowed(epsilon, width, height, position, walls, Up)) {
      s = s + (Up)
    }
    if (movementAllowed(epsilon, width, height, position, walls, Right)) {
      s = s + (Right)
    }
    if (movementAllowed(epsilon, width, height, position, walls, Down)) {
      s = s + (Down)
    }
    return s
  }
}
