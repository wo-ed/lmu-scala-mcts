package com.lmu.scala.mcts.games.pacman.actors

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode
import com.badlogic.gdx.graphics.g2d.{Animation, Batch, TextureRegion}
import com.badlogic.gdx.utils.Align
import com.lmu.scala.mcts.games.pacman.logic._
import com.lmu.scala.mcts.games.pacman.{PacManAssets, PacManMap}
import com.lmu.scala.mcts.gdxextensions.actors.AnimatedActor

class PacMan(map: PacManMap, assetManager: AssetManager) extends AnimatedActor with FourDirectionalMovingActor with Trigger {
  override var directionalVelocity = 1.0f
  private val startX = 13.5f * map.tileWidth
  private val startY = 7.0f * map.tileWidth

  private val walkAnimation: Animation[TextureRegion] = {
    val texture: Texture = assetManager.get(PacManAssets.pacMan)
    val regions: Array[TextureRegion] = TextureRegion.split(texture, 16, 16).flatten
    val animation = new Animation[TextureRegion](0.1f, regions: _*)
    animation.setPlayMode(PlayMode.LOOP)
    animation
  }

  override protected def activeAnimation: Animation[TextureRegion] = walkAnimation

  setSize(map.tileWidth * 2.0f, map.tileHeight * 2.0f)
  override def triggerWidth: Float = map.tileWidth
  override def triggerHeight: Float = map.tileHeight
  override val triggerOffsetX: Float = (getWidth - triggerWidth) / 2.0f
  override val triggerOffsetY: Float = (getHeight - triggerHeight) / 2.0f

  setOrigin(Align.center)

  triggerX = startX
  triggerY = startY

  def reset(): Unit = {
    direction = None
    triggerX = startX
    triggerY = startY
  }

  override def act(delta: Float): Unit = {
    super.act(delta)
    setRotation(nextRotation)
  }

  private def nextRotation: Float = direction match {
    case Some(Up) => -90.0f
    case Some(Down) => 90.0f
    case _ => 0.0f
  }

  override def flipX: Boolean = direction == Some(Right)

  override def draw(batch: Batch, parentAlpha: Float): Unit = super[AnimatedActor].draw(batch, parentAlpha)
}
