package com.lmu.scala.mcts.games.pacman.actors

import com.badlogic.gdx.scenes.scene2d.Actor

import com.lmu.scala.mcts.games.pacman.logic._

trait FourDirectionalMovingActor extends Actor {
  var direction: Option[Direction] = None
  var directionalVelocity: Float

  def velocityX: Float = direction match {
    case Some(Left) => -directionalVelocity
    case Some(Right) => directionalVelocity
    case _ => 0.0f
  }

  def velocityY: Float = direction match {
    case Some(Down) => -directionalVelocity
    case Some(Up) => directionalVelocity
    case _ => 0.0f
  }
}
