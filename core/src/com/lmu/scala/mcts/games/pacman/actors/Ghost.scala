package com.lmu.scala.mcts.games.pacman.actors

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode
import com.badlogic.gdx.graphics.g2d.{Animation, TextureRegion}
import com.lmu.scala.mcts.games.pacman.logic._
import com.lmu.scala.mcts.games.pacman.{PacManAssets, PacManMap}
import com.lmu.scala.mcts.gdxextensions.actors.AnimatedActor
import Ghost._

object GhostType extends Enumeration {
  val Red, Pink, Cyan, Orange = Value
}

object Behavior extends Enumeration {
  val Chase, Scatter = Value
}

object Ghost {
  val behaviorChangeFrameInterval: Int = 60 * 5 // fps * seconds
}

class Ghost(val ghostType: GhostType.Value, map: PacManMap, assetManager: AssetManager) extends AnimatedActor with FourDirectionalMovingActor with Trigger {
  var behavior = Behavior.Scatter
  var scatterPoint: (Int, Int) = map.randomPathPoint
  private var frameCounter = 0

  private val startX = ghostType match {
       case GhostType.Red => 13.5f * map.tileWidth
       case GhostType.Pink => 11.5f * map.tileWidth
       case GhostType.Cyan => 13.5f * map.tileWidth
       case GhostType.Orange => 15.5f * map.tileWidth
  }
  private val startY = ghostType match {
    case GhostType.Red => 19.0f * map.tileHeight
    case GhostType.Pink => 16.0f * map.tileHeight
    case GhostType.Cyan => 16.0f * map.tileHeight
    case GhostType.Orange => 16.0f * map.tileHeight
  }

  private val ghostsTexture: Texture = assetManager.get(PacManAssets.ghosts)
  private val ghostBlueTexture: Texture = assetManager.get(PacManAssets.ghostBlue)
  private val ghostWhiteTexture: Texture = assetManager.get(PacManAssets.ghostWhite)
  private val ghostEyesTexture: Texture = assetManager.get(PacManAssets.ghostEyes)

  private val walkTextureRegions: Array[TextureRegion] = {
    val allRegions = TextureRegion.split(ghostsTexture, 16, 16)
    allRegions(ghostType.id)
  }

  private val animationRight = new Animation[TextureRegion](0.1f, walkTextureRegions.slice(0, 2): _*)
  private val animationLeft = new Animation[TextureRegion](0.1f, walkTextureRegions.slice(2, 4): _*)
  private val animationUp = new Animation[TextureRegion](0.1f, walkTextureRegions.slice(4, 6): _*)
  private val animationDown = new Animation[TextureRegion](0.1f, walkTextureRegions.slice(6, 8): _*)
  private val animationBlue = new Animation[TextureRegion](0.1f, TextureRegion.split(ghostBlueTexture, 16, 16)(0): _*)
  private val animationWhite = new Animation[TextureRegion](0.1f, TextureRegion.split(ghostWhiteTexture, 16, 16)(0): _*)

  List(animationRight, animationLeft, animationUp, animationDown, animationBlue, animationWhite)
    .foreach(_.setPlayMode(PlayMode.LOOP))

  override protected def activeAnimation: Animation[TextureRegion] = direction match {
    case Some(Right) => animationRight
    case Some(Left) => animationLeft
    case Some(Up) => animationUp
    case Some(Down) | _ => animationDown
  }

  override var directionalVelocity: Float = 1.0f
  direction = Some(Left)

  setSize(14.0f, 14.0f)
  override def triggerWidth: Float = map.tileWidth
  override def triggerHeight: Float = map.tileHeight
  override val triggerOffsetX: Float = (getWidth - triggerWidth) / 2.0f
  override val triggerOffsetY: Float = (getHeight - triggerHeight) / 2.0f

  triggerX = startX
  triggerY = startY

  def reset(): Unit = {
    triggerX = startX
    triggerY = startY

    direction = Some(Left)
    behavior = Behavior.Scatter
    scatterPoint = map.randomPathPoint
    frameCounter = 0
  }

  override def act(delta: Float): Unit = {
    super.act(delta)
    frameCounter += 1
    if(frameCounter % behaviorChangeFrameInterval == 0) {
      behavior = Behavior(math.abs(behavior.id - 1))
    }
    if(behavior == Behavior.Scatter) {
      scatterPoint = fixScatterPoint(scatterPoint)
    }
  }

  /**
    * Checks if the ghost already is at the random scatter position
    * or if the position is inside the house. If that's the case,
    * we need a new position
    * @return a fixed scatter position that is not the ghost's position
    *         and not inside the house
    */
  private def fixScatterPoint(scatterPoint: (Int, Int)): (Int, Int) = {
    val (triggerCol, triggerRow) = map.convertPositionToTileIndices(triggerX, triggerY)
    var result = scatterPoint

    while((result._1 == triggerCol && result._2 == triggerRow) ||
      (map.houseColRange.contains(result._1) && map.houseRowRange.contains(result._2))) {
      result = map.randomPathPoint
    }
    result
  }
}
