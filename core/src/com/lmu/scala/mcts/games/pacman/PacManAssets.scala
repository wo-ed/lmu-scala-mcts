package com.lmu.scala.mcts.games.pacman

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.maps.tiled.TiledMap
import com.lmu.scala.mcts.gdxextensions.Assets

object PacManAssets extends Assets {
  val map = new AssetDescriptor[TiledMap]("pacman/map/map.tmx", classOf[TiledMap])
  val pacMan = new AssetDescriptor[Texture]("pacman/pacman.png", classOf[Texture])
  val foodSmall = new AssetDescriptor[Texture]("pacman/foodSmall.png", classOf[Texture])
  val foodBig = new AssetDescriptor[Texture]("pacman/foodBig.png", classOf[Texture])
  val ghosts = new AssetDescriptor[Texture]("pacman/ghosts/ghosts.png", classOf[Texture])
  val ghostBlue = new AssetDescriptor[Texture]("pacman/ghosts/ghost_blue.png", classOf[Texture])
  val ghostWhite = new AssetDescriptor[Texture]("pacman/ghosts/ghost_white.png", classOf[Texture])
  val ghostEyes = new AssetDescriptor[Texture]("pacman/ghosts/ghost_eyes.png", classOf[Texture])

  override val descriptors: List[AssetDescriptor[_]] = map :: pacMan :: foodSmall :: foodBig :: ghosts :: ghostBlue :: ghostWhite :: ghostEyes :: Nil
}
