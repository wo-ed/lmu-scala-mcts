package com.lmu.scala.mcts.games.pacman

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.maps.{MapLayer, MapObject}
import com.badlogic.gdx.maps.tiled.{TiledMap, TiledMapTileLayer}
import com.lmu.scala.mcts.gdxextensions.TiledMapImplicits._

import scala.collection.JavaConverters._
import com.badlogic.gdx.math.{Intersector, Rectangle}
import com.lmu.scala.mcts.games.pacman.actors.Food

import scala.util.Random

/**
  * A wrapper around the TMX tiled map to provide convenience methods & variables.
  * The .tmx map in the assets folder can be opened with a program called Tiled http://www.mapeditor.org/
  */
class PacManMap(assetManager: AssetManager) {
  val tiledMap: TiledMap = assetManager.get(PacManAssets.map)

  val metaLayer: TiledMapTileLayer = tiledMap.getLayers.get("MetaTiles").asInstanceOf[TiledMapTileLayer]
  val foodObjectLayer: MapLayer = tiledMap.getLayers.get("FoodObjectMap")

  val columns: Int = tiledMap.getProperties.get("width", classOf[Int])
  val tileWidth: Int = tiledMap.getProperties.get("tilewidth", classOf[Int])
  val width: Int = columns * tileWidth

  val rows: Int = tiledMap.getProperties.get("height", classOf[Int])
  val tileHeight: Int = tiledMap.getProperties.get("tileheight", classOf[Int])
  val height: Int = rows * tileHeight

  val wallRectangles: List[List[Option[Rectangle]]] = List.tabulate(columns, rows)(createWallRectangle)
  def flattenedWallRectangles: List[Rectangle] = wallRectangles.flatten.flatten

  val houseColRange: Range = 11 to 16
  val houseRowRange: Range = 15 to 17

  private def createWallRectangle(column: Int, row: Int): Option[Rectangle] =
    if (collides(column, row, rowsTopDown = false)) {
      Some(new Rectangle(column * tileWidth, row * tileHeight, tileWidth, tileHeight))
    }
    else {
      None
    }

  def createFoodObjects(): List[Food] = foodObjectLayer.getObjects.asScala.map(createFoodObject).toList

  private def createFoodObject(o: MapObject): Food = {
    val sFoodType: String = o.getProperties.get("type", classOf[String])
    val food = new Food(sFoodType, assetManager)
    food.setBounds(
      o.getProperties.get("x", classOf[Float]),
      o.getProperties.get("y", classOf[Float]),
      o.getProperties.get("width", classOf[Float]),
      o.getProperties.get("height", classOf[Float])
    )
    food
  }

  def convertPositionToTileIndices(x: Float, y: Float): (Int, Int) =
    ((x / tileWidth).toInt, (y / tileHeight).toInt)

  def randomPathPoint: (Int, Int) = {
    var rect: Option[Rectangle] = None
    var col = 0
    var row = 0
    do {
      col = Random.nextInt(columns)
      row = Random.nextInt(rows)
      rect = wallRectangles(col)(row)
    } while(rect.nonEmpty)

    (col, row)
  }

  /**
    * Checks if the collision rectangle of a game object collides with a wall tile
    *
    * @param rect Size and position of the collision rectangle
    * @param outRect Intersection of wall tile and game object
    * @return Whether the rectangles intersect
    */
  def intersectWithWall(rect: Rectangle, outRect: Rectangle): Boolean = {
    val first = flattenedWallRectangles.find(_.overlaps(rect))
    first.foreach(Intersector.intersectRectangles(_, rect, outRect))
    first.nonEmpty
  }

  def collides(column: Int, row: Int, rowsTopDown: Boolean = true): Boolean = {
    val s = metaLayer.getCell(column, row, rowsTopDown).getTile.getProperties.get("collide", classOf[String])
    // Android bug fix
    java.lang.Boolean.parseBoolean(s)
  }
}
