package com.lmu.scala.mcts.games.pacman.actors

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Batch
import com.lmu.scala.mcts.games.pacman.PacManAssets

object FoodType extends Enumeration {
  val Small, Big = Value
}

class Food(foodType: FoodType.Value, assetManager: AssetManager) extends Trigger {

  def this(sFoodType: String, assetManager: AssetManager) {
    this(sFoodType match {
      case "foodSmall" => FoodType.Small
      case "foodBig" => FoodType.Big
    }, assetManager)
  }

  private val texture = {
    val assetDescriptor = foodType match {
      case FoodType.Small => PacManAssets.foodSmall
      case FoodType.Big => PacManAssets.foodBig
    }
    assetManager.get(assetDescriptor)
  }

  override def triggerWidth: Float = getWidth
  override def triggerHeight: Float = getHeight
  override val triggerOffsetX = 0.0f
  override val triggerOffsetY = 0.0f

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)
    batch.draw(texture, getX, getY)
  }
}