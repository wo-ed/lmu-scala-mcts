package com.lmu.scala.mcts.games.connectfour

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.lmu.scala.mcts.gdxextensions.Assets

object ConnectFourAssets extends Assets {
  val Coin = new AssetDescriptor[Texture]("connectfour/coin.png", classOf[Texture])
  val CoinSlot = new AssetDescriptor[Texture]("connectfour/coin_slot.png", classOf[Texture])

  override val descriptors: List[AssetDescriptor[_]] = Coin :: CoinSlot :: Nil
}