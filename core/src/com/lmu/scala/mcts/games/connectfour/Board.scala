package com.lmu.scala.mcts.games.connectfour

import com.badlogic.gdx.assets.AssetManager
import com.lmu.scala.mcts.games.connectfour.Board._

object Board {
  val ColumnsDefault = 7
  val RowsDefault = 6
}

class Board(val columns: Int, val rows: Int, assetManager: AssetManager) {
  def this(assetManager: AssetManager) = {
    this(ColumnsDefault, RowsDefault, assetManager)
  }

  val slots: List[List[CoinSlot]] =
    List.tabulate(columns, rows) (new CoinSlot(_, _, assetManager))

  def reset(): Unit = slots.flatten.foreach(_.reset())

  def setCoin(column: Int, player: Int): Option[CoinSlot] =
    maxEmptyRowIndex(column).map(row => setCoin(column, row, player))

  private def setCoin(column: Int, row: Int, value: Int): CoinSlot = {
    val slot: CoinSlot = slots(column)(row)
    slot.value = Some(value)
    slot
  }

  def emptySlots(column: Int): List[CoinSlot] = slots(column).filterNot(_.isCoinSet)

  def maxEmptyRowIndex(column: Int): Option[Int] = emptySlots(column).indices.reduceOption(math.max)

  def isFull: Boolean = slots.flatten.forall(_.isCoinSet)
}
