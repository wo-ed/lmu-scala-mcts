package com.lmu.scala.mcts.games.connectfour

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.scenes.scene2d.Actor

class Grid(board: Board, assetManager: AssetManager) extends Actor {
  private val slotTexture: Texture = assetManager.get(ConnectFourAssets.CoinSlot)
  setColor(new Color(30.0f / 255.0f, 144.0f / 255.0f, 1.0f, 1.0f))

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    batch.setColor(getColor)
    board.slots.flatten.foreach(s => batch.draw(slotTexture, s.getX, s.getY, s.getWidth, s.getHeight))
  }
}
