package com.lmu.scala.mcts.games.connectfour

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.scenes.scene2d.actions.{Actions, MoveToAction}
import com.badlogic.gdx.scenes.scene2d.{Action, Actor}
import com.lmu.scala.mcts.games.connectfour.CoinSlot._
import com.lmu.scala.mcts.gdxextensions.actors.SpriteActor

object CoinSlot {
  val VelocityFactor: Float = 2.5f
}

class CoinSlot(val column: Int, val row: Int, assetManager: AssetManager) extends Actor {
  var value: Option[Int] = None

  val coin = new SpriteActor {
    sprite.setRegion(assetManager.get[Texture](ConnectFourAssets.Coin))
  }

  def isCoinSet: Boolean = value.isDefined

  override def act(delta: Float): Unit = {
    super.act(delta)
    coin.act(delta)
  }

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    if (isCoinSet) {
      coin.setSize(getWidth, getHeight)
      coin.draw(batch, parentAlpha)
    }
  }

  def reset(): Unit = {
    value = None
    coin.setColor(Color.WHITE)
  }

  def activateCoin(color: Color, afterAction: () => Unit): Unit = {
    coin.setColor(color)

    val targetY: Float = getY
    val startY: Float = getStage.getViewport.getWorldHeight
    val deltaY: Float = startY - targetY
    val velocity = getParent.getHeight * VelocityFactor
    coin.setPosition(getX, startY)

    val action = new MoveToAction
    action.setPosition(coin.getX, targetY)
    action.setDuration(deltaY / velocity)

    coin.addAction(Actions.sequence(action, new Action {
      override def act(delta: Float): Boolean = {
        afterAction()
        true
      }
    }))
  }
}
