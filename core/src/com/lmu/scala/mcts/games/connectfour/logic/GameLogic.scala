package com.lmu.scala.mcts.games.connectfour.logic

abstract class Disk
case object Empty extends Disk
case object Player1 extends Disk
case object Player2 extends Disk


abstract class Rating
case object Won extends Rating
case object Lost extends Rating
case class Rate(val n: Int) extends Rating

object GameLogic {
  type Board = Vector[Vector[Disk]]
  type Player = Int
  type Move = Int

  val winPositionss = Vector(
    // Vertical winning positions
    ((0, 0), (0, 1), (0, 2), (0, 3)),
    ((1, 0), (1, 1), (1, 2), (1, 3)),
    ((2, 0), (2, 1), (2, 2), (2, 3)),
    ((3, 0), (3, 1), (3, 2), (3, 3)),
    ((4, 0), (4, 1), (4, 2), (4, 3)),
    ((5, 0), (5, 1), (5, 2), (5, 3)),
    ((6, 0), (6, 1), (6, 2), (6, 3)),

    ((0, 1), (0, 2), (0, 3), (0, 4)),
    ((1, 1), (1, 2), (1, 3), (1, 4)),
    ((2, 1), (2, 2), (2, 3), (2, 4)),
    ((3, 1), (3, 2), (3, 3), (3, 4)),
    ((4, 1), (4, 2), (4, 3), (4, 4)),
    ((5, 1), (5, 2), (5, 3), (5, 4)),
    ((6, 1), (6, 2), (6, 3), (6, 4)),

    ((0, 2), (0, 3), (0, 4), (0, 5)),
    ((1, 2), (1, 3), (1, 4), (1, 5)),
    ((2, 2), (2, 3), (2, 4), (2, 5)),
    ((3, 2), (3, 3), (3, 4), (3, 5)),
    ((4, 2), (4, 3), (4, 4), (4, 5)),
    ((5, 2), (5, 3), (5, 4), (5, 5)),
    ((6, 2), (6, 3), (6, 4), (6, 5)),

    // Horizontal winning positions
    ((0, 0), (1, 0), (2, 0), (3, 0)),
    ((1, 0), (2, 0), (3, 0), (4, 0)),
    ((2, 0), (3, 0), (4, 0), (5, 0)),
    ((3, 0), (4, 0), (5, 0), (6, 0)),

    ((0, 1), (1, 1), (2, 1), (3, 1)),
    ((1, 1), (2, 1), (3, 1), (4, 1)),
    ((2, 1), (3, 1), (4, 1), (5, 1)),
    ((3, 1), (4, 1), (5, 1), (6, 1)),

    ((0, 2), (1, 2), (2, 2), (3, 2)),
    ((1, 2), (2, 2), (3, 2), (4, 2)),
    ((2, 2), (3, 2), (4, 2), (5, 2)),
    ((3, 2), (4, 2), (5, 2), (6, 2)),

    ((0, 3), (1, 3), (2, 3), (3, 3)),
    ((1, 3), (2, 3), (3, 3), (4, 3)),
    ((2, 3), (3, 3), (4, 3), (5, 3)),
    ((3, 3), (4, 3), (5, 3), (6, 3)),

    ((0, 4), (1, 4), (2, 4), (3, 4)),
    ((1, 4), (2, 4), (3, 4), (4, 4)),
    ((2, 4), (3, 4), (4, 4), (5, 4)),
    ((3, 4), (4, 4), (5, 4), (6, 4)),

    ((0, 5), (1, 5), (2, 5), (3, 5)),
    ((1, 5), (2, 5), (3, 5), (4, 5)),
    ((2, 5), (3, 5), (4, 5), (5, 5)),
    ((3, 5), (4, 5), (5, 5), (6, 5)),

    //Diagonal \ winning positions
    ((0, 5), (1, 4), (2, 3), (3, 2)),
    ((0, 4), (1, 3), (2, 2), (3, 1)),
    ((0, 3), (1, 2), (2, 1), (3, 0)),

    ((1, 5), (2, 4), (3, 3), (4, 2)),
    ((1, 4), (2, 3), (3, 2), (4, 1)),
    ((1, 3), (2, 2), (3, 1), (4, 0)),

    ((2, 5), (3, 4), (4, 3), (5, 2)),
    ((2, 4), (3, 3), (4, 2), (5, 1)),
    ((2, 3), (3, 2), (4, 1), (5, 0)),

    ((3, 5), (4, 4), (5, 3), (6, 2)),
    ((3, 4), (4, 3), (5, 2), (6, 1)),
    ((3, 3), (4, 2), (5, 1), (6, 0)),

    //Diagonal / winning positions
    ((0, 0), (1, 1), (2, 2), (3, 3)),
    ((0, 1), (1, 2), (2, 3), (3, 4)),
    ((0, 2), (1, 3), (2, 4), (3, 5)),

    ((1, 0), (2, 1), (3, 2), (4, 3)),
    ((1, 1), (2, 2), (3, 3), (4, 4)),
    ((1, 2), (2, 3), (3, 4), (4, 5)),

    ((2, 0), (3, 1), (4, 2), (5, 3)),
    ((2, 1), (3, 2), (4, 3), (5, 4)),
    ((2, 2), (3, 3), (4, 4), (5, 5)),

    ((3, 0), (4, 1), (5, 2), (6, 3)),
    ((3, 1), (4, 2), (5, 3), (6, 4)),
    ((3, 2), (4, 3), (5, 4), (6, 5))
  )

  def initialState(): Board =
    Vector(
      Vector(Empty, Empty, Empty, Empty, Empty, Empty),
      Vector(Empty, Empty, Empty, Empty, Empty, Empty),
      Vector(Empty, Empty, Empty, Empty, Empty, Empty),
      Vector(Empty, Empty, Empty, Empty, Empty, Empty),
      Vector(Empty, Empty, Empty, Empty, Empty, Empty),
      Vector(Empty, Empty, Empty, Empty, Empty, Empty),
      Vector(Empty, Empty, Empty, Empty, Empty, Empty)
    )

  def play(player: Int, diskss: Board, m: Move): Option[Board] = {
    val disks = diskss(m)
    val p = player match { case 1 => Player1 case 2 => Player2 }
    disks.indexOf(Empty) match {
      case -1 => None
      case i => Some(diskss.updated(m, disks.updated(i, p)))
    }
  }

  def movements(diskss: Board): Set[Move] = {
    for (((x0, y0), (x1, y1), (x2, y2), (x3, y3)) <- winPositionss) {
      val d0 = diskss(x0)(y0)
      val d1 = diskss(x1)(y1)
      val d2 = diskss(x2)(y2)
      val d3 = diskss(x3)(y3)
      (d0, d1, d2, d3) match {
        case (Player1, Player1, Player1, Player1) => return Set.empty
        case (Player2, Player2, Player2, Player2) => return Set.empty
        case _ => ()
      }
    }

    diskss.zipWithIndex.foldLeft(Set[Int]()) { case (s, (disks, i)) =>
      disks(5) match {
        case Empty => s union Set(i)
        case _ => s
      }
    }
  }

  def rateState(player: Int, diskss: Board): Rating = {
    for (((x0, y0), (x1, y1), (x2, y2), (x3, y3)) <- winPositionss) {
      val d0 = diskss(x0)(y0)
      val d1 = diskss(x1)(y1)
      val d2 = diskss(x2)(y2)
      val d3 = diskss(x3)(y3)
      (player, d0, d1, d2, d3) match {
        case (1, Player1, Player1, Player1, Player1) => return Won
        case (2, Player1, Player1, Player1, Player1) => return Lost
        case (1, Player2, Player2, Player2, Player2) => return Lost
        case (2, Player2, Player2, Player2, Player2) => return Won
        case _ => ()
      }
    }

    for (((x0, y0), (x1, y1), (x2, y2), (x3, y3)) <- winPositionss) {
      val d0 = diskss(x0)(y0)
      val d1 = diskss(x1)(y1)
      val d2 = diskss(x2)(y2)
      val d3 = diskss(x3)(y3)
      (player, d0, d1, d2, d3) match {

        case (1, Empty,   Player1, Player1, Player1) => return Rate(1)
        case (1, Player1, Empty,   Player1, Player1) => return Rate(1)
        case (1, Player1, Player1, Empty,   Player1) => return Rate(1)
        case (1, Player1, Player1, Player1, Empty  ) => return Rate(1)

        case (2, Empty,   Player2, Player2, Player2) => return Rate(1)
        case (2, Player2, Empty,   Player2, Player2) => return Rate(1)
        case (2, Player2, Player2, Empty,   Player2) => return Rate(1)
        case (2, Player2, Player2, Player2, Empty  ) => return Rate(1)

        case (1, Empty,   Player2, Player2, Player2) => return Rate(-1)
        case (1, Player2, Empty,   Player2, Player2) => return Rate(-1)
        case (1, Player2, Player2, Empty,   Player2) => return Rate(-1)
        case (1, Player2, Player2, Player2, Empty  ) => return Rate(-1)

        case (2, Empty,   Player1, Player1, Player1) => return Rate(-1)
        case (2, Player1, Empty,   Player1, Player1) => return Rate(-1)
        case (2, Player1, Player1, Empty,   Player1) => return Rate(-1)
        case (2, Player1, Player1, Player1, Empty  ) => return Rate(-1)

        case _ => ()
      }
    }

    Rate(0)
  }

  def rateStateFloat(player: Int, diskss: Board): Float =
    rateState(player, diskss) match {
      case Won     => 10.0f
      case Lost    => -10.0f
      case Rate(_) => 0
  }
}
