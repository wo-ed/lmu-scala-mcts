package com.lmu.scala.mcts.games.connectfour

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui._
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.{Gdx, InputAdapter, ScreenAdapter}
import com.lmu.scala.mcts.games.connectfour.ConnectFourScreen._
import com.lmu.scala.mcts.games.connectfour.logic.GameLogic.Move
import com.lmu.scala.mcts.games.connectfour.logic._
import com.lmu.scala.mcts.gdxextensions.AssetManagerImplicits._
import com.lmu.scala.mcts.gdxextensions.Clearable
import com.lmu.scala.mcts.gdxextensions.input.ListenerImplicits._
import com.lmu.scala.mcts.main.{MainMenuScreen, MonteCarloTreeSearchGame}
import com.lmu.scala.mcts.planner.{FPlanner, MctsPlanner, State, VanillaMonteCarloPlanner}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Random

object ConnectFourScreen {
  val colorPlayer1 = Color.YELLOW
  val colorPlayer2 = Color.RED
  val colorWinningPlayer = Color.PURPLE
}

class ConnectFourScreen(game: MonteCarloTreeSearchGame) extends ScreenAdapter with Clearable {
  private val board = new Board(game.assetManager)

  override protected var bgColor: Color = new Color(20.0f / 255.0f, 20.0f / 255.0f, 1.0f, 1.0f)

  private val gameStage = new Stage(new FitViewport(board.columns * 128, board.rows * 128), game.batch)

  private val inputAdapter = new InputAdapter {
    override def keyDown(keycode: Int): Boolean = keycode match {
      case Keys.BACK | Keys.BACKSPACE | Keys.ESCAPE => exit(); true
      case _ => false
    }
  }

  private var currentPlayer = randomPlayer()
  private var currentBoard = GameLogic.initialState()

  type A = GameLogic.Move
  type S = State.Adapter[(GameLogic.Player, GameLogic.Board), A]

  private val planner: FPlanner[(GameLogic.Player, GameLogic.Board), GameLogic.Move] = new MctsPlanner[S,A](
    iterations = 50000,
    horizon = 3,
    new MctsPlanner.BellmanUpdateStrategy[S,A](1, 1.0),
    MctsPlanner.uctPolicy[S,A](130.0),
    MctsPlanner.uctPolicy[S,A](0.0),
    MctsPlanner.uctMinSamplePolicy[S,A](130.0))

  private val gameTable = new Table(game.defaultSkin)
  gameTable.setFillParent(true)

  private val grid = new Grid(board, game.assetManager)

  private val label = new Label("", game.defaultSkin)
  label.setWrap(true)
  label.setFontScale(0.8f)
  label.setAlignment(Align.center)

  private val restartButton = new TextButton("Restart", game.defaultSkin)
  restartButton.addClickListener((_, _, _) => restart())

  private val dialog = new Dialog("", game.defaultSkin)
  dialog.setModal(true)
  dialog.setVisible(false)
  dialog.getContentTable.add(label).width(200)
  dialog.getButtonTable.add(restartButton)
  dialog.pack()

  private val processingLabel = new Label("Processing...", game.defaultSkin)
  processingLabel.setAlignment(Align.center)
  processingLabel.setFontScale(2.0f)
  processingLabel.setFillParent(true)

  private val coinPercentHeight: Float = 1.0f / board.rows
  private val coinSize: Value = Value.percentHeight(coinPercentHeight, gameTable)

  private var gameOver = false
  private var animationFinished = true
  private var processing = false
  private var disposed = false

  addActors()

  private def exit(): Unit = {
    dispose()
    game.setScreen(new MainMenuScreen(game))
  }

  private def restart(): Unit = {
    board.reset()
    dialog.setVisible(false)
    currentPlayer = randomPlayer()
    currentBoard = GameLogic.initialState()
    gameOver = false
  }

  private def randomPlayer(): Int = Random.nextInt(2) + 1

  private def addActors(): Unit = {
    gameStage.addActor(gameTable)
    addCoinSlotsToGameStage()
    gameStage.addActor(grid)
    gameStage.addActor(processingLabel)
    dialog.show(gameStage)
  }

  private def addCoinSlotsToGameStage(): Unit =
    board.slots.flatten.sortBy(slot => (slot.row, slot.column)).foreach(addCoinSlot)

  private def addCoinSlot(slot: CoinSlot): Unit = {
    gameTable.add(slot).size(coinSize)

    val column: Int = slot.column
    if (column == board.columns - 1) {
      gameTable.row()
    }

    slot.addClickListener((_, _, _) => {
      if (readyForMove && currentPlayer == 1) {
        if (GameLogic.movements(currentBoard).contains(column)) {
          currentBoard = GameLogic.play(currentPlayer, currentBoard, column).get
          setCoin(column)
        }
      }
    })
  }

  override def render(delta: Float): Unit = {
    clearScreen()

    if (board.isFull) {
      showGameOverMessage("Draw")
    }

    processingLabel.setVisible(processing)

    renderStage(gameStage, delta)

    if (readyForMove && currentPlayer == 2) {
      computerMove()
    }
  }

  private def renderStage(stage: Stage, delta: Float): Unit = {
    stage.getViewport.apply(true)
    stage.act(delta)
    stage.draw()
  }

  private def readyForMove: Boolean = animationFinished && !gameOver && !processing

  private def showGameOverMessage(message: String): Unit = {
    label.setText(message)
    dialog.setVisible(true)
    gameOver = true
  }

  // TODO: Monte Carlo Tree Search
  private def computerMove(): Unit = {
    processing = true

    val future: Future[Move] = Future {
      /*
      def planner: FPlanner[(GameLogic.Player, GameLogic.Board), GameLogic.Move] =
        new VanillaMonteCarloPlanner[
          State.Adapter[(GameLogic.Player, GameLogic.Board), GameLogic.Move],
          GameLogic.Move](iterations = 50000, horizon = 3, gamma = 1.0)
      */

      val movement = planner.bestAction(
        { case (player, board) => GameLogic.rateStateFloat(player, board) },
        { case (_, board) => GameLogic.movements(board).to[Seq] },
        { case ((player, board), move) =>
          GameLogic.play(player, board, move) match {
            case Some(board2) => {
              var newPlayer = player % 2 + 1
              var states = GameLogic.movements(board2).map(move =>
                (player, GameLogic.play(newPlayer, board2, move).get)).to[Vector]
              if (states.isEmpty)
                Vector((player, board2))
              else
                states
            }
            case None => Vector()
          }
        }, (currentPlayer, currentBoard)) match {
        case Some(action) => action
        case None => throw new Exception()
      }

      currentBoard = GameLogic.play(currentPlayer, currentBoard, movement).get
      movement
    }

    future.onSuccess {
      case movement =>
        if (!disposed) {
          setCoin(movement)
          processing = false
        }
    }

    future.onFailure {
      case ex =>
        Gdx.app.log("ConnectFourScreen", "computer move failed", ex)
        processing = false
    }
  }

  private def setCoin(columnIndex: Int): Unit = {
    GameLogic.rateState(1, currentBoard) match {
      case Won => showGameOverMessage("Won")
      case Lost => showGameOverMessage("Lost")
      case Rate(_) => ()
    }

    board.setCoin(columnIndex, currentPlayer).foreach(slot => activateCoin(slot))
    currentPlayer = currentPlayer % 2 + 1
  }


  private def activateCoin(slot: CoinSlot): Unit = {
    animationFinished = false
    slot.activateCoin(playerColor, () => animationFinished = true)
  }

  private def playerColor: Color = currentPlayer match {
    case 1 => colorPlayer1
    case 2 => colorPlayer2
  }

  override def resize(width: Int, height: Int): Unit = gameStage.getViewport.update(width, height, true)

  override def show(): Unit = {
    game.inputMultiplexer.addProcessor(inputAdapter)
    game.inputMultiplexer.addProcessor(gameStage)
  }

  override def dispose(): Unit = {
    disposed = true
    game.assetManager.unload(ConnectFourAssets)
    game.inputMultiplexer.removeProcessor(inputAdapter)
    game.inputMultiplexer.removeProcessor(gameStage)
    gameStage.dispose()
  }
}
