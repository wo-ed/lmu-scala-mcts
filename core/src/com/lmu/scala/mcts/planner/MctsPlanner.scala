package com.lmu.scala.mcts.planner

import com.lmu.scala.mcts.planner.MctsPlanner.{_}

import scala.util.Random

/**
  * A Monte Carlo Tree Search (MCTS) planner
  */
class MctsPlanner[S <: State[A, S], A](iterations : Int = 200,
                                       horizon : Int = 3,
                                       updateStrategy: UpdateStrategy[S,A]
                                          = new MctsPlanner.BellmanUpdateStrategy[S,A](2, 0.9),
                                       treePolicy : Policy[S,A] = MctsPlanner.uctPolicy(3.0),
                                       evaluationPolicy : Policy[S,A] = MctsPlanner.uctPolicy(0.0),
                                       samplePolicy : SamplePolicy[S,A] = MctsPlanner.uniformSamplePolicy,
                                       cacheDecisionTree : Boolean = true)
  extends Planner[S, A]{

  private var rootStateNode : Option[StateNode[S,A]] = None

  private def expand(node : StateNode[S,A]): Unit =
    node.actions = Some(node.state.validActions.map(a => {
      val action = new ActionNode[S,A](a,
        node.state.successorStates(a).map(s => new StateNode[S,A](s, s.reward)),
        node)
      action.successors.foreach(successor => successor.parent = Some(action))
      action
      }
    ))

  private def select(stateNode : StateNode[S,A], depth : Int) : (StateNode[S,A], Int) = {
    if (depth == horizon)
      return (stateNode, depth)

    stateNode.actions match {
      case None => {
        expand(stateNode)
        select(stateNode, depth)
      }
      case Some(actions) => actions match {
        case Nil => (stateNode, depth)
        case validActions => validActions.filter(a => a.visitCount == 0) match {
          case Nil => {
            val selectedAction = treePolicy(stateNode)
            val successor = selectedAction.successors.filter(s => s.visitCount == 0) match {
              case Nil => samplePolicy(selectedAction)
              case untriedSuccessors => sampleUniform(untriedSuccessors)
            }
            select(successor, depth + 1)
          }
          case untriedActions => {
            val selectedAction = sampleUniform(untriedActions)
            val successor = sampleUniform(selectedAction.successors)
            (successor, depth + 1)
          }
        }
      }
    }
  }

  private def createStateNode(state: S) : StateNode[S,A] = {
    lazy val stateNode = new StateNode[S, A](state, state.reward)

    cacheDecisionTree match {
      case true => stateNode
      case false => rootStateNode match {
        case Some(cachedRoot) => {
          val successorStateNodes = cachedRoot.actions.get.flatMap(a => a.successors)
          val existingStateNode = successorStateNodes.find(s => s.state == state)
          rootStateNode = Some(existingStateNode.getOrElse(stateNode))
          rootStateNode.get
        }
        case None => {
          rootStateNode = Some(stateNode)
          stateNode
        }
      }
    }
  }

  override def bestAction(state: S): Option[A] = {
    val root = createStateNode(state)

    for(i <- 0 to iterations) {
      val (selectedNode, depth) = select(root, 0)

      if (selectedNode == root)
        return None

      val reward = updateStrategy.rollout(selectedNode, depth)
      updateStrategy.backup(selectedNode, reward, depth)
    }

    /*
    println("Actions:")
    root.actions.get.foreach(a => println(" (" + a.action + ") value = "
     + a.rewardSum + ", visitCount = " + a.visitCount))
    */
    Some(evaluationPolicy(root).action)
  }

}

object MctsPlanner{
  def sampleUniform[T](seq: Seq[T]): T = seq(Random.nextInt(seq.size))

  type Policy[S, A] = StateNode[S, A] => ActionNode[S, A]

  type SamplePolicy[S,A] = ActionNode[S,A] => StateNode[S,A]

  trait UpdateStrategy[S <: State[A, S],A]{

    def rollout(leaf: StateNode[S,A], depth: Int) : Float

    def backup(leaf: StateNode[S,A], reward: Float, depth: Int) : Unit
  }

  class StateNode[S, A](val state : S, val reward : Float){
    var visitCount = 0
    var rewardSum = 0.0f
    var actions : Option[Seq[ActionNode[S, A]]] = None
    var parent : Option[ActionNode[S,A]] = None
  }

  class ActionNode[S, A](val action : A, val successors : Seq[StateNode[S, A]], val parent : StateNode[S,A]){
    var visitCount = 0
    var rewardSum = 0.0f
  }

  def uctPolicy[S, A](c: Double) : Policy[S,A] = stateNode => stateNode.actions.get.maxBy { a =>
    a.rewardSum / a.visitCount + c * Math.sqrt(2*Math.log(stateNode.visitCount) / a.visitCount)
  }

  def greedyPolicy[S, A] : Policy[S,A] = stateNode => stateNode.actions.get.maxBy { a =>
    a.rewardSum
  }

  def uniformSamplePolicy[S,A] : SamplePolicy[S,A] = actionNode => sampleUniform(actionNode.successors)

  def uctMinSamplePolicy[S,A](c: Double) : SamplePolicy[S,A] = actionNode => actionNode.successors.minBy { s =>
    s.rewardSum / s.visitCount - c * Math.sqrt(2*Math.log(actionNode.visitCount) / s.visitCount)
  }

  class BellmanUpdateStrategy[S <: State[A, S],A](lookahead: Int, gamma: Double)
    extends UpdateStrategy[S,A]{

    private def rollout(state: S, depth: Int, reward: Float, maxDepth : Int) : Float = depth match {
      case `maxDepth` => reward
      case _ => state.validActions match {
        case Nil => reward
        case actions => {
          val action = sampleUniform(actions)
          val successorState = sampleUniform(state.successorStates(action))
          val currentReward = scala.math.pow(gamma, depth).toFloat * successorState.reward
          rollout(successorState, depth + 1, reward + currentReward, maxDepth)
        }
      }
    }

    override def rollout(leaf: StateNode[S, A], depth: Int): Float =
      rollout(leaf.state, depth, 0.0f, depth+lookahead)

    private def backup(actionNode: ActionNode[S,A], reward: Float, depth : Int) : Unit = {
      actionNode.visitCount += 1
      actionNode.rewardSum += reward
      backup(actionNode.parent, reward, depth - 1)
    }

    override def backup(stateNode: StateNode[S,A], reward: Float, depth : Int) : Unit = {
      stateNode.visitCount += 1
      val updatedReward = reward + scala.math.pow(gamma, depth).toFloat * stateNode.reward
      stateNode.rewardSum += updatedReward
      stateNode.parent match {
        case Some(parent) => backup(parent, updatedReward, depth)
        case None => //We have reached the root
      }
    }
  }

}