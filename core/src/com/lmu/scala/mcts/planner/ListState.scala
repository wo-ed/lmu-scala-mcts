package com.lmu.scala.mcts.planner

/**
  * Simple state that represents an integer list. With each transition a new integer is added to the list until
  * maxLength is reached. Yields a reward if the sum of the elements in the final state is even.
  */
class ListState(val list: Seq[Int]) extends State[Int, ListState]{
  def maxLength = 10

  override def reward = list.foldLeft(0)(_ + _) % 2 match {
    case 0 if list.length >= maxLength => 10.0f
    case _ => 0.0f
  }

  override def validActions = list.length match {
    case length if length >= maxLength => Nil
    case _ => 0 to 9
  }

  override def successorStates(action: Int) = List(new ListState(list :+ action))

  override def toString: String = list.toString
}

object ListState{
  def main(args: Array[String]): Unit = {
    var currentState = new ListState(Vector(3))
    val planner = new VanillaMonteCarloPlanner[ListState, Int](iterations = 500, horizon = 5)

    while(currentState.validActions.nonEmpty){
      val action = planner.bestAction(currentState)
      println("State: " + currentState + " - Best Action: " + action)
      currentState = currentState.successorStates(action.get).head
    }

    println("Final State: " + currentState + " - Reward: " + currentState.reward)
  }
}