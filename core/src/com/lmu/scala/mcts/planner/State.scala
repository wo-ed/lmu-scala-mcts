package com.lmu.scala.mcts.planner

/**
  * A state in a markov decision process with actions of type A
  * <p>
  * To support returning successor states of exactly the same type, each implementing class has to supply its own type
  * as a type parameter
  *
  * @tparam A the action type
  * @tparam S the type of successor states
  */
trait State[A, S <: State[A, S]] {

  /**
    * Returns the reward associated with this state
    * @return reward
    */
  def reward : Float

  /**
    * Returns the actions that are possible from this state
    * @return valid actions or empty sequence if final state
    */
  def validActions : Seq[A]

  /**
    * Returns the states that can be reached when applying the specified action. These states are
    * assumed to be uniformly distributed.
    * @param action a valid action from this state
    * @return reachable successor states given the specified action (may not be empty for
    *         actions returned from [[com.lmu.scala.mcts.planner.State#validActions]])
    */
  def successorStates(action: A) : Seq[S]
}

object State{

  /**
    * An adapter class that can be used to make arbitrary types conform to the State interface
    */
  implicit class Adapter[S, A](val state: S)(implicit val rewardFunction: S => Float,
                                             val validActionFunction: S => Seq[A],
                                             val successorStateFunction: (S, A) => Seq[S])
    extends State[A, Adapter[S, A]]{

    override def reward: Float = rewardFunction(state)

    override def validActions: Seq[A] = validActionFunction(state)

    override def successorStates(action: A): Seq[Adapter[S, A]] = successorStateFunction(state, action) map {
      new Adapter[S, A](_) }

    override def equals(obj: scala.Any): Boolean = obj match{
      case adapter : Adapter[S,A] => state.equals(adapter.state)
      case _ => false
    }
  }
}