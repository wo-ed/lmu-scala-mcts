package com.lmu.scala.mcts.planner

/**
  * An object-oriented style interface for planners
  * <p>
  * Type parameters are declared at trait scope instead of function scope because implementations might want
  * to cache the decision tree inside the object
  */
trait Planner[S <: State[A, S], A] {

  def bestAction(state: S) : Option[A]

}

object Planner{

  /**
    * An adapter class to convert from FPlanner objects to Planner objects
    */
  implicit class FPlannerAdapter[S <: State[A, S], A](fPlanner: FPlanner[S, A]) extends Planner[S, A]{

    override def bestAction(state: S): Option[A] = {

      def rewardFunction (s: S) = s.reward
      def validActionFunction (s: S) = s.validActions
      def successorStatesFunction (s: S, a: A) = s.successorStates(a)

      fPlanner.bestAction(rewardFunction, validActionFunction, successorStatesFunction, state)
    }
  }
}