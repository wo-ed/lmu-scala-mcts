package com.lmu.scala.mcts.planner

import scala.util.Random

/**
  * A vanilla monte carlo planner with uniform action selection and no tree growth
  */
class VanillaMonteCarloPlanner[S <: State[A, S], A](iterations : Int = 200,
                                                    horizon : Int = 3,
                                                    gamma : Double = 0.9)
  extends Planner[S, A]{

  private def sampleUniform[T](seq: Seq[T]): T = seq(Random.nextInt(seq.size))

  private def simulate(state: S, depth: Int, reward: Double) : Double = depth match {
    case `horizon` => reward
    case _ => state.validActions match {
      case Nil => reward
      case actions => {
        val action = sampleUniform(actions)
        val successorState = sampleUniform(state.successorStates(action))
        val currentReward = scala.math.pow(gamma, depth) * successorState.reward
        simulate(successorState, depth + 1, reward + currentReward)
      }
    }
  }

  private def actionRewards(state: S, actions: Seq[A]) : Seq[(A, Double)] =
    actions map (action => action -> {
      (1 to (iterations / actions.size)).foldLeft(0.0)((reward, _) => {
        val currentState = sampleUniform(state.successorStates(action))
        reward + simulate(currentState, 1, currentState.reward)
      })
    })

  override def bestAction(state: S): Option[A] = state.validActions match {
      case Nil => None
      case actions => Some(actionRewards(state, actions).maxBy(_._2)._1)
    }
}
