package com.lmu.scala.mcts.planner

/**
  * A functional style interface for planners
  * <p>
  * Type parameters are declared at trait scope instead of function scope because implementations might want
  * to cache the decision tree inside the object
  */
trait FPlanner[S, A] {

  def bestAction(reward: S => Float,
                 validActions: S => Seq[A],
                 successorStates:  (S, A) => Seq[S],
                 initialState: S) : Option[A]

}

object FPlanner{

  /**
    * An adapter class to convert from Planner objects to FPlanner objects
    */
  implicit class PlannerAdapter[S, A](planner: Planner[State.Adapter[S, A], A]) extends FPlanner[S,A]{

    override def bestAction(reward: (S) => Float,
                            validActions: (S) => Seq[A],
                            successorStates: (S, A) => Seq[S],
                            initialState: S): Option[A] = {
      planner.bestAction(new State.Adapter[S,A](initialState)(reward, validActions, successorStates))
    }
  }
}