package com.lmu.scala.mcts.gdxextensions

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{Color, GL20}

trait Clearable {
  protected var bgColor: Color

  protected def clearScreen(): Unit = {
    Gdx.gl.glClearColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
  }
}
