package com.lmu.scala.mcts.gdxextensions.input

import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent}
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import scala.language.implicitConversions

// These implicits make creating listeners look cleaner
object ListenerImplicits {

  implicit class clickableActor(actor: Actor) {
    def addClickListener(func: (InputEvent, Float, Float) => Unit): Boolean = actor.addListener(new ClickListener() {
      override def clicked(event: InputEvent, x: Float, y: Float): Unit = func(event, x, y)
    })
  }

  implicit def clicked(handler: ClickedEvent => Unit): ClickListener = new ClickListener() {
    override def clicked(inputEvent: InputEvent, x: Float, y: Float): Unit = {
      val clickEvent: ClickedEvent = new ClickedEvent(inputEvent, x, y)
      handler(clickEvent)
    }
  }
}
