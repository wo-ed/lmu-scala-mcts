package com.lmu.scala.mcts.gdxextensions.input

import com.badlogic.gdx.scenes.scene2d.InputEvent

class BackKeyUpEvent(val inputEvent: InputEvent, val keyCode: Int)
class ClickedEvent(val inputEvent: InputEvent, val x: Float, val y: Float)