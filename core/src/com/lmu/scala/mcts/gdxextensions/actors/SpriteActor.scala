package com.lmu.scala.mcts.gdxextensions.actors

import com.badlogic.gdx.graphics.g2d.{Batch, Sprite}
import com.badlogic.gdx.scenes.scene2d.Actor

trait SpriteActor extends Actor {
  protected val sprite = new Sprite()
  protected def flipX = false
  protected def flipY = false

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    sprite.setBounds(getX, getY, getWidth, getHeight)
    sprite.setRotation(getRotation)
    sprite.setOrigin(getOriginX, getOriginY)
    sprite.setScale(getScaleX, getScaleY)
    sprite.setColor(getColor)

    sprite.flip(flipX, flipY)
    sprite.draw(batch, parentAlpha)
    sprite.flip(flipX, flipY)
  }
}
