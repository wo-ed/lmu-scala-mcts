package com.lmu.scala.mcts.gdxextensions.actors

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.{Animation, Batch, TextureRegion}

trait AnimatedActor extends SpriteActor {
  protected def activeAnimation: Animation[TextureRegion]
  private var counter = 0.0f

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    counter += Gdx.graphics.getDeltaTime
    val textureRegion = activeAnimation.getKeyFrame(counter)
    sprite.setRegion(textureRegion)
    super.draw(batch, parentAlpha)
  }
}
