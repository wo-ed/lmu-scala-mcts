package com.lmu.scala.mcts.gdxextensions

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell

object TiledMapImplicits {
  implicit class richTiledMapTileLayer(layer: TiledMapTileLayer) {
    def getCell(x: Int, y: Int, topDown: Boolean): Cell = layer.getCell(x, reverseY(y, topDown))
    def tryGetCell(x: Int, y: Int): Option[Cell] = Option(layer.getCell(x, y))
    def tryGetCell(x: Int, y: Int, topDown: Boolean): Option[Cell] = tryGetCell(x, reverseY(y, topDown))

    private def reverseY(row: Int, topDown: Boolean): Int = if (topDown) layer.getHeight - row - 1 else row
  }
}
