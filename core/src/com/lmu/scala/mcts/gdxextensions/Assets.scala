package com.lmu.scala.mcts.gdxextensions

import com.badlogic.gdx.assets.{AssetDescriptor, AssetManager}

object AssetManagerImplicits {
  implicit class richAssetManager(assetManager: AssetManager) {
    def load(assets: Assets): Unit = assets.load(assetManager)
    def unload(assets: Assets): Unit = assets.unload(assetManager)
  }
}

trait Assets {
  val descriptors: Traversable[AssetDescriptor[_]]

  def load(assetManager: AssetManager): Unit = descriptors.foreach(assetManager.load)
  def unload(assetManager: AssetManager): Unit = descriptors.map(d => d.fileName).foreach(assetManager.unload)
}
