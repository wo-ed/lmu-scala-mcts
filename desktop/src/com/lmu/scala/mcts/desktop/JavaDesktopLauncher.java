package com.lmu.scala.mcts.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.lmu.scala.mcts.main.MonteCarloTreeSearchGame;

public class JavaDesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.fullscreen = false;
        config.width = 0;
        config.height = 0;
        new LwjglApplication(new MonteCarloTreeSearchGame(), config);
    }
}
