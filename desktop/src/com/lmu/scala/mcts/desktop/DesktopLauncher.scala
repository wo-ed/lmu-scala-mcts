package com.lmu.scala.mcts.desktop

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.lmu.scala.mcts.main.MonteCarloTreeSearchGame

object DesktopLauncher {
  def main(arg: Array[String]) {
    val config: LwjglApplicationConfiguration = new LwjglApplicationConfiguration
    config.fullscreen = false
    config.width = 0
    config.height = 0
    new LwjglApplication(new MonteCarloTreeSearchGame, config)
  }
}